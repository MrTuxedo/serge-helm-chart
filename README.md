# Serge AI Helm Chart

[![License: WTFPL](https://img.shields.io/badge/License-WTFPL-brightgreen.svg)](http://www.wtfpl.net/about/)
[![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/serge)](https://artifacthub.io/packages/search?repo=serge)

This Helm chart deploys a LLaMA based chat interface named Serge.
https://github.com/nsarrazin/serge

## Prerequisites

- Kubernetes 1.12+ or OpenShift 3.11+
- Helm 3+

## Installing the Chart

To install the chart with the release name `serge-ai`:

#### Enable helm OCI support
```sh
export HELM_EXPERIMENTAL_OCI=1
```

#### Install the chart
```sh
helm install serge-ai oci://registry-1.docker.io/mrtuxedo/serge --version 0.0.3 
```

See the [configuration](#configuration) section for information on how to customize the installation.

## Uninstalling the Chart

To uninstall the chart with the release name serge-ai, run:

```sh
helm uninstall serge-ai
```

## Configuration

The following table lists the configurable parameters of the chart and their default values. For more information, see the values.yaml file.

|Parameter	|Description	|Default|
--- | --- | --- |
|image.tag|Target branch for Serge |latest|
|environment.timezone	|Timezone Identifier for deployment|	Europe/Amsterdam|
|persistence.datadb.size|Volume size for chat history storage|16Gi|
|persistence.weights.size|Volume size for model weights storage|64Gi|

To override any of the default values, create a YAML file with your desired overrides and pass it to the helm install command using the --values flag. For example:

> Remember to [enable helm OCI support](#enable-helm-oci-support)

```sh
helm install serge-ai oci://registry-1.docker.io/mrtuxedo/serge --version 0.0.3 --values my-values.yaml
```

To set default values from the `install` command:

```sh
helm install serge-ai oci://registry-1.docker.io/mrtuxedo/serge --version 0.0.3 \
  --namespace serge-ai \
  --create-namespace \
  --set environment.timezone=Antarctica/DumontDUrville
```

For more information on configuring the chart, see the values.yaml file and the official Helm documentation.

## Contributing

Thank you for considering contributing to this Helm chart! Pull requests and bug reports are always welcome. Chart source repository can be found [here](https://git.ssh.surf/MrTuxedo/serge-helm-chart).

Before submitting a pull request, please ensure that:

  - Your changes are well-tested and conform to the Helm chart best practices and conventions.
  - Your changes are clearly documented in the pull request description and/or the chart's README.md file.
  - You have run the `helm lint` and `helm template` commands to validate your chart and ensure that it can be rendered correctly.

For major changes, please open an issue first to discuss your proposed changes.

For more information on contributing to Helm charts, see the [official Helm documentation](https://helm.sh/docs/).


### License

[WTFPL](./LICENSE)